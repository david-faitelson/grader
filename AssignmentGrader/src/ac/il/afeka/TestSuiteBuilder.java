package ac.il.afeka;

import junit.framework.TestSuite;

public interface TestSuiteBuilder {

	public TestSuite buildTestSuite(Class assignmentClass);
}
