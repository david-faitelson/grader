package ac.il.afeka;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLClassLoader;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import ac.il.afeka.Submission.Submission;

public class Grader {

	public static void main(String[] args) throws Exception {

		TestSuiteBuilder builder = (TestSuiteBuilder)Class.forName("TestBuilder").newInstance();
		 
	 	JUnitCore c = new JUnitCore();

		System.out.println("id\tvalid\tfailures\ttotal\tgrade");

		for(File file : new File(args[0]).listFiles()) {
			
			if (isJar(file)) {
				
				URLClassLoader submissionLoader = new URLClassLoader(new URL[]{file.toURI().toURL()});
				
				try {
					Class submissionClass = submissionLoader.loadClass("Main");
				
					Submission submission = (Submission)submissionClass.newInstance();
	
					Result result = c.run(builder.buildTestSuite(submissionClass));  
	
					if (result.getFailureCount() > 0) {
						createReportFile(file.getPath(), result);
					}
					
					if (submission.submittingStudentIds().size() > 3) {
						throw new Exception("Too many submitters (only 3 allowed)");
					}
					else {
						for(String id : submission.submittingStudentIds()) {
							System.out.println(id + "\t" + "yes" + "\t" + result.getFailureCount() + "\t" + result.getRunCount() + "\t" + (result.getRunCount()-result.getFailureCount())/new Float(result.getRunCount())*100);
						}
					}
				}
				catch(Exception err) {
					createInvalidSubmissionReportFile(file.getPath(), err);
				}
				finally {
					submissionLoader.close();
				}
			}
		}
	}

	private static void createInvalidSubmissionReportFile(String submissionFilename, Exception err) throws FileNotFoundException {
		
		String reportFilename = submissionFilename.substring(0, submissionFilename.lastIndexOf('.')) + "Report.txt";
		
		File reportFile = new File(reportFilename);
		
		PrintStream out = new PrintStream(reportFile); 
		
		out.println("Invalid submission");
		
		err.printStackTrace(out);
		
		out.close();	
	}

	private static void createReportFile(String submissionFilename, Result result) throws FileNotFoundException {
		
		String reportFilename = submissionFilename.substring(0, submissionFilename.lastIndexOf('.')) + "Report.txt";
	
		File reportFile = new File(reportFilename);
		
		PrintStream out = new PrintStream(reportFile); 
		
		for(Failure failure : result.getFailures()) {
			out.print("The test ");
			out.print(failure.getTestHeader());
			out.print(" failed because ");
			if (failure.getMessage() != null)
				out.println(failure.getMessage());
			out.println("");
			out.println("Stack trace:");
			out.println(failure.getTrace());
		}	
		
		out.close();
	}
	
	private static boolean isJar(File file) {
		String name = file.getName();
		int index = name.lastIndexOf('.');
		if (index == -1) return false;
		return name.substring(index+1).equals("jar");
	}

}
