package ac.il.afeka.Submission;

import java.util.List;

public interface Submission {
	
	public List<String> submittingStudentIds();
	
}
